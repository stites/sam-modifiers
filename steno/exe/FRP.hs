{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE Arrows #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecursiveDo #-}
module FRP where

import Cli(RunOpts(..))
import Control.Monad
import Data.Maybe (isJust, fromJust)
import Data.List (nub)
import System.Random
import System.IO as IO
import Debug.Trace
import Data.IORef
import Data.ByteString
import qualified Data.ByteString.Char8 as BIO (putStrLn)
-- import qualified Data.ByteString.UTF8 as BIO (fromString)
import Network.Socket
import Network.Socket.ByteString

import Data.MonadicStreamFunction

import Reactive.Banana (Event, Behavior)
import Reactive.Banana.Frameworks (MomentIO, AddHandler)
import qualified Reactive.Banana as R
import qualified Reactive.Banana.Frameworks as R

runNetwork :: RunOpts  -> IO ()
runNetwork _ = mkSource "/tmp/" >>= eventLoop

mkSource :: FilePath -> IO Socket
mkSource base = do
  sock <- socketIO
  connect sock (SockAddrUnix (base <> "/stenomachine.sock"))
  pure sock
  where
    socketIO = socket AF_UNIX Stream defaultProtocol

eventLoop :: Socket → IO ()
eventLoop s = loop where
  loop = do
    continue:[] <- embed network [()]
    if continue then loop else pure ()

  network :: MSF IO () Bool
  network = proc () -> do
    rpt      <- constM (recv s 64) -< ()
    ()       <- arrM BIO.putStrLn  -< rpt
    returnA <<< constM (pure True) -< ()
