module Cli where

import Options.Applicative


data Opts = Opts
  { cmd :: Command
  }

data Command
  = Analyze AnalyzeOpts
  | Run RunOpts

data AnalyzeOpts = AnalyzeOpts
  { file :: FilePath
  }
data RunOpts = RunOpts

analyzeOpts :: Parser AnalyzeOpts
analyzeOpts = AnalyzeOpts
    <$> strOption
      ( long "file"
          <> metavar "FILE"
          <> help "Json dictionary to parse"
      )

runOpts :: Parser RunOpts
runOpts = pure RunOpts

opts :: Parser Opts
opts =
  Opts
    <$> subparser
      ( command "analyze" (info (Analyze <$> analyzeOpts) (progDesc "analyze a dictionary file"))
     <> command "run" (info (Run <$> runOpts) (progDesc "run a plover clone"))
      )
