{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecursiveDo #-}

module Main where

import Cli
import FRP(runNetwork)
import Data.Steno.Input
import Options.Applicative
import Parse.Json

import Control.Monad
import Data.Maybe (isJust, fromJust)
import Data.List (nub)
import System.Random
import System.IO
import Debug.Trace
import Data.IORef

import Data.MonadicStreamFunction

import Reactive.Banana as R
import Reactive.Banana.Frameworks as R

main :: IO ()
main = runCommand =<< execParser opts
  where
    opts =
      info
        (Cli.opts <**> helper)
        ( fullDesc
            <> progDesc "CLI for working with Json"
            <> header "steno - various stenography tools"
        )

readjson :: AnalyzeOpts -> IO ()
readjson a = do
  putStrLn $ show $ mkStroke [Tl, Pl, F, Pr :: StenoKey]
  putStrLn $ show $ readOutline "TP-FP"
  readJsonPairs (file a) >>= \case
    Left err -> putStrLn err
    Right kv -> flip mapM_ kv $ \(k, v) ->
      case readOutline k of
        Left err -> putStrLn $ "error for " ++ k ++ ":" ++ err
        Right o -> putStrLn $ show o ++ ": " ++ v

runCommand :: Opts -> IO ()
runCommand Opts{cmd} =
  case cmd of
    Analyze opts -> readjson opts
    Run opts -> runNetwork opts
