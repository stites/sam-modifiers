{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}

-- |
-- I consider a "Chord" part of theory, and the rest part of input. Following:
--
--     ,-------- Chord (group of keys representing a sound (TPH = initial N)
--    |     _.-- Stroke separator (/)
--    |   .'  ,- Stroke  (group of keys all pressed at once)
--   _|_  |__|_
--  STPHOG/TPEU
--  -----------
--      |
--      '------- Outline (stroke, or group of strokes that translate into a word or phrase)
--
-- Re: https://cdn.discordapp.com/attachments/136953735426473984/1100150590996221952/Screenshot_2023-04-24_at_4.05.34_PM.png
module Data.Steno.Input where

-- import Control.Applicative (Alternative (..))
import Control.Monad (void)
import Data.HashSet (HashSet)
import qualified Data.HashSet as HS
import Data.Hashable (Hashable)
import Data.List (partition, sort)
import Data.Void (Void)
import GHC.Generics (Generic)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Debug

type StenoKey = StenoKeyX Void Void

data StenoKeyX pre suf
  = ExtL pre
  | Num
  | Sl
  | Tl
  | K
  | Pl
  | W
  | H
  | Rl
  | A
  | O
  | Star
  | E
  | U
  | F
  | Rr
  | Pr
  | B
  | L
  | G
  | Tr
  | Sr
  | D
  | Z
  | ExtR pre
  deriving (Eq, Ord, Generic)

type Ext p s = (Show p, Show s, Ord p, Ord s, Eq p, Eq s, Hashable p, Hashable s)

instance (Hashable pre, Hashable suf) => Hashable (StenoKeyX pre suf)

instance (Show pre, Show suf) => Show (StenoKeyX pre suf) where
  show (ExtL p) = show p
  show Num = "#"
  show Sl = "S-"
  show Tl = "T-"
  show K = "K"
  show Pl = "P-"
  show W = "W"
  show H = "H"
  show Rl = "R-"
  show A = "A"
  show O = "O"
  show Star = "*"
  show E = "E"
  show U = "U"
  show F = "F"
  show Rr = "-R"
  show Pr = "-P"
  show B = "B"
  show L = "L"
  show G = "G"
  show Tr = "-T"
  show Sr = "-S"
  show D = "D"
  show Z = "Z"
  show (ExtR p) = show p

render :: (Show pre, Show suf) => StenoKeyX pre suf -> String
render Sl = "S"
render Tl = "T"
render Pl = "P"
render Rl = "R"
render Rr = "R"
render Pr = "P"
render Tr = "T"
render Sr = "S"
render key = show key

needsDisambiguation :: StenoKeyX pre suf -> Bool
needsDisambiguation = \case
  Sl -> True
  Tl -> True
  Pl -> True
  Rl -> True
  Rr -> True
  Pr -> True
  Tr -> True
  Sr -> True
  _ -> False

onLeft :: (Ord p, Ord s) => StenoKeyX p s -> Bool
onLeft key = key < Star

isStar :: (Eq p, Eq s) => StenoKeyX p s -> Bool
isStar key = key == Star

onRight :: (Ord p, Ord s) => StenoKeyX p s -> Bool
onRight key = key > Star

type Stroke = StrokeX Void Void

newtype StrokeX pre suf = Stroke (HashSet (StenoKeyX pre suf))
  deriving (Eq)

mkStroke :: Ext p s => [StenoKeyX p s] -> Maybe (StrokeX p s)
mkStroke [] = Nothing
mkStroke ks = Just . Stroke . HS.fromList $ ks

toList :: Ext p s => StrokeX p s -> [StenoKeyX p s]
toList (Stroke c) = sort $ HS.toList c

split :: Ext p s => StrokeX p s -> ([StenoKeyX p s], Bool, [StenoKeyX p s])
split c =
  let lst = toList c
      (leftKeys, rightFull) = partition onLeft lst
      ([mstar], rightKeys) = splitAt 1 rightFull
   in if mstar == Star
        then (leftKeys, True, rightKeys)
        else (leftKeys, False, rightFull)

instance Ext pre suf => Show (StrokeX pre suf) where
  show c@(Stroke keys) =
    if not (Star `HS.member` keys) && any needsDisambiguation keys
      then case split c of
        ([], False, []) -> error "empty chord is impossible!"
        ([], False, rs) -> "-" ++ concatMap render rs
        (ls, False, []) -> concatMap render ls ++ "-"
        (ls, False, rs) -> (concatMap render ls) <> "-" <> (concatMap render rs)
        _ -> error "impossible"
      else concatMap render (toList c)

-- * parser section

type Parser =
  Parsec
    -- The type for custom error messages. We have none, so use `Void`.
    Void
    -- The input stream type. Let's use `String` for now, but for
    -- better performance, you might want to use `Text` or `ByteString`.
    String

parseLR :: Bool -> StenoKey -> StenoKey -> String -> Parser StenoKey
parseLR isLeft l r str = (if isLeft then l else r) <$ string' str

-- parseKeyX :: Parser p -> Parser s -> Bool -> Parser (StenoKeyX p s)
-- parseKeyX extl extr isLeft = choice

data Side = LeftSide | RightSide
  deriving (Eq, Show)

parseKey :: Side -> Parser StenoKey
parseKey side =
  -- dbg ("key(" ++ show side ++ ")") $
  choice
    -- [ ExtL <$> extl
    [ Num <$ string' "#",
      parseLR isLeft Sl Sr "S",
      parseLR isLeft Tl Tr "T",
      K <$ string' "K",
      parseLR isLeft Pl Pr "P",
      W <$ string' "W",
      H <$ string' "H",
      parseLR isLeft Rl Rr "R",
      A <$ string' "A",
      O <$ string' "O",
      Star <$ string' "*",
      E <$ string' "E",
      U <$ string' "U",
      F <$ string' "F",
      B <$ string' "B",
      L <$ string' "L",
      G <$ string' "G",
      D <$ string' "D",
      Z <$ string' "Z"
      -- , ExtR extr
    ]
  where
    isLeft = case side of
      LeftSide -> True
      RightSide -> False

parseStrokeLeft :: Parser [StenoKey]
parseStrokeLeft = manyTill (parseKey LeftSide) $ void foundRight

-- parses until it finds a token belonging, uniquely, to the right
-- side, or until is finds a '-' character
foundRight :: Parser String
foundRight =
  lookAhead . choice $
    [ (string' "-"),
      (string' "E"),
      (string' "U"),
      (string' "F"),
      (string' "B"),
      (string' "L"),
      (string' "G"),
      (string' "D"),
      (string' "Z")
    ]

parseStrokeRight :: Parser [StenoKey]
parseStrokeRight = some (parseKey RightSide)

parseStroke :: Parser Stroke
parseStroke = do
  ls <- -- dbg "parseStrokeLeft" $
    parseStrokeLeft

  -- should cover everything up to * on the left
  void $ optional (string' "-")

  rs <- -- dbg "parseStrokeRight" $
    parseStrokeRight

  pure . Stroke $ HS.fromList (ls <> rs)

readStroke :: String -> Either String Stroke
readStroke s = case parse parseStroke "<input>" s of
  Left bundle -> Left $ errorBundlePretty bundle
  Right c -> Right c

strokeSeperator :: String
strokeSeperator = "/"

strokeSeperatorParser :: Parser ()
strokeSeperatorParser = void (string' "/")

type Outline = OutlineX Void Void

newtype OutlineX pre suf = Outline [StrokeX pre suf]
  deriving (Eq, Show)

parseOutline :: Parser Outline
parseOutline = fmap Outline $ do
  void $ optional (string' "\"")
  x <- sepBy parseStroke strokeSeperatorParser
  void $ optional (string' "\"")
  pure x

readOutline :: String -> Either String Outline
readOutline s = case parse parseOutline "<input>" s of
  Left bundle -> Left $ errorBundlePretty bundle
  Right c -> Right c
