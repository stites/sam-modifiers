module Parse.Json where

import Data.Aeson (Value (..), eitherDecode)
import qualified Data.Aeson as Json
import qualified Data.Aeson.KeyMap as Json
import Data.Aeson.Types (Parser, parseEither)
import qualified Data.ByteString.Lazy as B
import qualified Data.Text as T

readJsonPairs :: FilePath -> IO (Either String [(String, String)])
readJsonPairs fp = do
  contents <- B.readFile fp
  pure $ do
    v <- eitherDecode contents
    parseEither parsePairs v

parsePairs :: Value -> Parser [(String, String)]
parsePairs (Object obj) = sequenceA $ parsePair <$> Json.toList obj
parsePairs _ = fail "failed to parse json object"

parsePair :: (Json.Key, Value) -> Parser (String, String)
parsePair (k, String v) = pure (show k, T.unpack v)
parsePair _ = fail "failed to parse pair"
