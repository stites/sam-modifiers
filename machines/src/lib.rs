use hs_bindgen::*;

extern crate hidapi;

use hidapi::HidApi;

#[hs_bindgen]
pub fn hello(name: &str) {
    println!("Hello, {name}!");
}

#[hs_bindgen]
pub fn print_hid_devices() {
    println!("Printing all available hid devices:");

    match HidApi::new() {
        Ok(api) => {
            for device in api.device_list() {
                // println!("{:04x}:{:04x}\t{:?}\t{:?}\t{:?}", device.vendor_id(), device.product_id(), device.path(), device.manufacturer_string(), device.product_string() );
                println!("{}:{}\t{:?}\t{:?}\t{:?}", device.vendor_id(), device.product_id(), device.path(), device.manufacturer_string(), device.product_string() );
            }
        },
        Err(e) => {
            eprintln!("Error: {}", e);
        },
    }
}

