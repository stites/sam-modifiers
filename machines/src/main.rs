use hidapi::{HidError,HidDevice,HidResult,HidApi};
use std::env;
use std::fs;
use std::io::Write;
use std::os::unix::net::{UnixListener};

const USAGE_PAGE : u16 = 0xFF50;
const USAGE: u16 = 0x4C56;

// A simple report contains the report id 1 and one bit
// for each of the 64 buttons in the report.
const N_LEVERS: usize = 64;

pub fn find_device(api: &HidApi) -> HidResult<HidDevice> {
    let info = api.device_list()
        .find(|dev| {
            dev.usage() == USAGE &&
            dev.usage_page() == USAGE_PAGE
        });

    match info {
        None => Err(HidError::HidApiError {message : "no plover-hid device found!".to_string() }),
        Some(info) => {
            println!("Found Plover-HID device:");
            println!("VendorId:       {}", info.vendor_id());
            println!("ProductId:      {}", info.product_id());
            println!("Path:           {:?}", info.path());
            println!("Manufacturer:   {:?}", info.manufacturer_string());
            println!("Product String: {:?}", info.product_string());
            info.open_device(api)
        }
    }
}

pub fn proxy_reports(keeb:&HidDevice, send_all: bool, log: bool) {
    let mut socket = env::temp_dir();
    socket.push(r"stenomachine.sock");
    // fs::create_dir_all(socket.clone()).ok();
    // socket.push(r"socket");
    println!("socket: {}", socket.display());

    // Delete old socket if necessary
    if socket.exists() {
        fs::remove_file(&socket).unwrap();
    }
    // Bind to socket
    let listener = match UnixListener::bind(&socket) {
        Err(_) => panic!("failed to bind socket"),
        Ok(listener) => listener,
    };

    println!("Server started, waiting for clients");

    // Iterate over clients, blocks if no client available
    for stream in listener.incoming() {
        match stream {
            Ok(mut stream) => {
                loop {
                    let mut buf = [0u8; N_LEVERS];
                    let res = keeb.read(&mut buf[..]).unwrap();
                    if send_all || buf[0] == 80 {
                        stream.write_all(buf.as_slice()).expect("failed to write into stream!");
                        stream.flush().expect("flush failed!");

                        if log {
                            let mut data_string = String::new();
                            for u in &buf[..res] {
                                data_string.push_str(&(u.to_string() + " "));
                            }
                            println!("{}", data_string);
                        }
                    }
                }
            }
            Err(err) => {
                println!("{:?}", err);
                // connection failed
                break;
            }
        }
    }

}

fn main() {
    let api = HidApi::new().expect("Failed to create API instance");
    let keeb = find_device(&api).expect("keyboard to be found");
    proxy_reports(&keeb, true, true);
    // nc -U /tmp/stenomachine.sock to connect
}
