{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";

    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.url = "nixpkgs";
  };

  outputs = inputs@{ self, nixpkgs, flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = nixpkgs.lib.systems.flakeExposed;
      perSystem = { self', config, inputs', pkgs, lib, system, ... }: {
        packages.default = config.packages.stenomachines_rs;
        packages.stenomachines_rs = (pkgs.callPackage inputs.naersk { }).buildPackage {
          src = ./.;
          copyLibs = true;
          nativeBuildInputs = with pkgs; [ pkg-config hidapi systemd.dev ];
        };
      };
    };
}
